const rootReducer = (
  objectState = { number: 0 },
  action = { type: "", payload: 0 }
) => {
  //console.log("INCREASE", action, objectState);
 // if (isNaN(objectState)) objectState = { number: 0 };
  switch (action.type) {
    case "INCREASE":
     // return objectState.number + action.payload;
      objectState = { ...objectState, number: objectState.number + action.payload };

      break;
    case "DECREASE":
        objectState = { ...objectState, number: objectState.number - action.payload };

      break;
    default:
      //return objectState.number + action.payload;
      break;
  }
  return objectState;
}

export default rootReducer;
