import React, { Component, lazy, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Notfoundpage from "./404page";

export class Authenpage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      path: this.props.path,
      component: this.props.component,
      key: this.props.key,
      route: () => <div></div>,
    };
  }

  componentDidMount() {
    //if (this.props.key >= 1) {
    this.setState((state) => ({
      route: () => (
        <Suspense fallback={<div>Loading...</div>}>
          <Route
            path={this.state.path}
            component={this.state.component}
            key={this.state.key}
          />
        </Suspense>
      ),
    }));
    console.log("this.state", this.state);
    // } else {
    //   this.setState((state) => ({
    //     route: () => <Notfoundpage {...this.props} />,
    //   }));
    // }
  }

  render() {
    return <>{this.state.route()}</>;
  }
}

export default Authenpage;
