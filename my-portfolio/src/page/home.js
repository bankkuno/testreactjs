import react, { Component } from "react";
import Header from "../components/header";
import Menu from "../components/menu";
import Content from "../components/content";
import Footer from "../components/footer";

class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {


  }


  render(){
      return (
        <div>
        <Header></Header>
        <Menu></Menu>
        <Content></Content>
        <Footer></Footer>
       </div>
      );
  }
}

export default Home;
