import React, { Component } from "react";
import { useHistory } from "react-router-dom";

import { connect } from "react-redux";

import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import AccountCircle from "@material-ui/icons/AccountCircle";

import "../css/login.css";

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formObj: { UserName: "", Password: "" },
      isuserLogin: true,
      isPassword: false,
      componentShow: () => <div></div>,
      number: this.props.number,
    };
  }

  componentDidMount() {
    if (this.state.isuserLogin) {
    }
    this.setState((prevstate) => ({
      ...prevstate,
      number: this.props.number,
    }));
  }

  redirectToHome = () => {
    this.props.history.push("/portfolio");
    var _number = this.props.number || 0;
    this.props.increase(_number + 5);

    this.setState((prevstate) => ({
      ...prevstate,
      number: _number + 5,
    }));
  };

  enterlogin = (event, _component) => {
    if (event.key === "Enter") {
      switch (_component) {
        case "userName":
          this.setState((prevstate) => ({
            isuserLogin: !prevstate.isuserLogin,
          }));
          this.setState((prevstate) => ({ isPassword: !prevstate.isPassword }));
          break;
        case "password":
          this.setState((prevstate) => ({ isPassword: !prevstate.isPassword }));
          this.setState((prevstate) => ({
            isuserLogin: !prevstate.isuserLogin,
          }));
          break;

        default:
          break;
      }
    }
  };

  chageUserName = (event) => {
    this.setState((prevState) => ({
      formObj: {
        ...prevState.formObj,
        UserName: event.target.value,
      },
    }));
  };

  chagePassword = (event) => {
    this.setState((prevState) => ({
      formObj: {
        ...prevState.formObj,
        Password: event.target.value,
      },
    }));
  };

  render() {
    return (
      <div className="box">
        <div className="loginbox">
          <div className="loginbox2">
            <div className="imgprofile">
              <img
                className="imgprofile"
                src="http://www.myiconfinder.com/uploads/iconsets/256-256-5d8cab7b01ffef290b73909d06d92705.png"
              ></img>
            </div>
            <div className="form-login">
              <form className="formroot" noValidate autoComplete="off">
                <div className="form-box">
                  <TextField
                    label="User Name"
                    variant="filled"
                    id="form-username"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <AccountCircle />
                        </InputAdornment>
                      ),
                    }}
                    value={this.state.formObj.UserName}
                    onChange={(ev) => {
                      this.chageUserName(ev);
                    }}
                  />
                </div>
                <div className="form-box">
                  <TextField
                    id="outlined-password-input"
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    variant="filled"
                  />
                </div>
              </form>
            </div>
            <div className="boxsubmit">
              <Button
                variant="contained"
                className="btn-box"
                size="large"
                color="secondary"
                onClick={(ev) => {
                  this.redirectToHome();
                }}
              >
                Secondary
              </Button>

              <Button
                variant="contained"
                className="btn-box"
                size="large"
                color="primary"
              >
                Primary
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  console.log("state", state);
  return {
    number: state.rootReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => ({
  increase: (value) => {
    console.log("state1", value);
    return dispatch({ type: "INCREASE", payload: value });
  },
  decrease: (value) => {
    console.log("state2", value);
    return dispatch({ type: "DECREASE", payload: value });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
// export default Login;
