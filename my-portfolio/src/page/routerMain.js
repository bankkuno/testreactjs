import Mainlogin from "../components/mainlogin";
import Title from "../components/title";
import Home from "../page/home";
import Portfolio from "../page/portfolio";
import Notfoundpage from "./404page";
import Form from "./form";
import Login from "./login";
import Upload from './upload';


export const lst_mainRouter = [
  {
    path: "/home",
    component: Home,
    keys: 1,
  },
  {
    path: "/portfolio",
    component: Portfolio,
    keys: 2,
  },
  // { path: "/login", component: Login, keys: 3 },

  {
    path: "/404page",
    component: Notfoundpage,
    keys: 5,
  },
  { path: "/form", component: Form, keys: 7 },
  { path: "/title", component: Title, keys: 8 },
  { path: "/mainlogin", component: Mainlogin, keys: 9 },
  { path: "/upload", component: Upload, keys: 10 },
  
  {
    path: "/",
    component:Mainlogin,
    keys: 4,
  },
];

export default { lst_mainRouter };
