import React, { Component,lazy,Suspense } from "react";

import "../css/portfolio.css";
import "animate.css/animate.min.css";
import "../css/timeline.css";

import ScrollAnimation from "react-animate-on-scroll";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
// import Timeline from "../components/timeline";
import Chart from "../components/chart";

const Timeline = React.lazy(() => import('../components/timeline'));

library.add(fas, fab, far);

export class portfolio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      infochart: 
        {
          title: "My Skills ",
          data: [
            { x: 'C# .net', y: 70 },
            { x: 'React', y: 50 },
            { x: 'SqlServer', y: 60 },
            { x: 'Angularjs', y: 60 },
            { x: 'Ionic3', y: 50 },
            { x: '.Net Core', y: 50 },
            { x: 'Excel VBA', y: 40 },
           
          ],
        },
      
    };
  }

  render() {
    return (
      <div>
        <div className="header">
          <div className="headerline">
            <img
              src="http://imgc.allpostersimages.com/images/P-473-488-90/68/6896/2GOJ100Z/posters/despicable-me-2-minions-movie-poster.jpg"
              className="profileImg"
            />
            <h1>
              <FontAwesomeIcon icon={["far", "user"]}></FontAwesomeIcon> Mr.
              Paripat Wongsamut
            </h1>
            <h3>Programer</h3>
            <p>
              <q>
                The background-image property sets one or more background images
                for an element. By default, a background-image is placed at the
                top-left corner of an element, and repeated both vertically and
                horizontally. Tip: The background of an element is the total
                size of the element, including padding and border (but not the
                margin). Tip: Always set a background-color to be used if the
                image is unavailable.{" "}
              </q>
            </p>
          </div>
        </div>
        <div className="profileInfo">
          <div className="information">
            <ScrollAnimation animateIn="fadeIn" duration={5}>
              <h1>
                <FontAwesomeIcon icon={["fas", "address-card"]} /> infomation
              </h1>
            </ScrollAnimation>
          </div>

          <div className="history">
            <ScrollAnimation animateIn="fadeIn" duration={5}>
              <h1>
                <FontAwesomeIcon icon={["fas", "graduation-cap"]} />
                History
              </h1>
              <Suspense fallback={<div>Loading...</div>}>
              <Timeline />
              </Suspense>
            </ScrollAnimation>
          </div>
        </div>
        <div className="profileInfo">
          <div className="information">
            <ScrollAnimation animateIn="fadeIn" duration={5}>
              <h1>
                <Chart {...this.state} />
              </h1>
            </ScrollAnimation>
          </div>
          <div className="information">
          
          </div>
          <div className="information">
          
          </div>
        </div>
        <footer></footer>
      </div>
    );
  }
}

export default portfolio;
