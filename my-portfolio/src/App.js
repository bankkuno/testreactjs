import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { lst_mainRouter } from "./page/routerMain";
import React, { Component, lazy, Suspense } from "react";

const Authenpage = React.lazy(() => import("./page/authenpage"));
//import Authenpage from "./page/authenpage";

class App extends Component {
  constructor(props) {
    super(props);
    console.log(lst_mainRouter);
  }
  render() {
    const routeComponents = lst_mainRouter.map(
      ({ path, component, keys }, key) => {
        return <Authenpage path={path} component={component} key={keys} />;
      }
    );

    return (
      <div id="app" className="App">
        <Suspense fallback={<div>Loading...</div>}>
          <Router basename="/bankkuno/my-portfolio">
            <Switch>{routeComponents}</Switch>
          </Router>
        </Suspense>
      </div>
    );
  }
}

export default App;
