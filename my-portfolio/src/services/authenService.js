const userLst = [{ UserName: "Admin", Password: "1234" }];

const AuthService = {
  loginService: async function (userModel) {
    var userLogin = userLst.find(
      (user) =>
        user.UserName== userModel.UserName &&
        user.Password == userModel.Password
    );
    if (userLogin !== undefined && userLogin != null) {
      return userLogin;
    }

    return null;
  },
};

export default AuthService;
