import React, { useCallback, useEffect } from "react";
import CanvasJSReact from "../assets/canvasjs.react";
//var CanvasJSReact = require('./canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default function Chart(props) {
 var chart = {} ;
  var options = {
    title: {
      animationEnabled: true,
      exportEnabled: true,
      theme: "light2",
      text: "Basic Column Chart in React",
    },
    axisY: {
      includeZero: true,
    },
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    data: [
      {
        type: "column",
        dataPoints: [],
      },
    ],
  };

  useEffect(() => {
    console.log("this.props", props);
    //var chart = chart;
    chart.options.data[0].dataPoints = props.infochart.data;
    chart.options.data[0].dataPoints.push({x:'test', y: 25 - Math.random() * 10}) //= props.infochart.data;

    
    chart.render();
  });

  return (
    <div>
      <CanvasJSChart
        options={options}
        onRef={(ref) => (chart = ref)}
        /* onRef = {ref => this.chart = ref} */
      />
    </div>
  );
}
