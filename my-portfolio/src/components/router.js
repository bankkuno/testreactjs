import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  useHistory,
} from "react-router-dom";
import About from "../page/about";
import Login from "../page/login";

class RoutePage extends Component {
     path  = useRouteMatch().path;
     url  = useRouteMatch().url;
  constructor(props) {
    super(props);
    this.state = {
        route:[]


    }
    
  }


  componentDidMount() {
    this.setState(... [
        {
          path: `${this.path}/about`,
          exact: true,
          sidebar: () =><h2>Home</h2>,
          main: () =>  <About></About>
        },
        {
          path: `${this.path}/login`,
          sidebar: () =><h2>Home</h2>,
          main: () =>  <Login></Login>
        }
      ])

  }

  render() {
    return (
      <div>
        <Router >
          <Switch>
            {this.state.route.map((route, index) => (
              // You can render a <Route> in as many places
              // as you want in your app. It will render along
              // with any other <Route>s that also match the URL.
              // So, a sidebar or breadcrumbs or anything else
              // that requires you to render multiple things
              // in multiple places at the same URL is nothing
              // more than multiple <Route>s.
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
               
              />
            ))}
          </Switch>
        </Router>
      </div>
    );
  }
}

RoutePage.propTypes = {};

export default RoutePage;
