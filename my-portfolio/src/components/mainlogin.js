import React, { useState, useCallback } from "react";
import { useHistory } from "react-router-dom";
import AuthService from "../services/authenService";
import "../css/mainlogin.css";

export default function Mainlogin(props) {
  let [userLogin, setuserLogin] = useState({ Username: "", Password: "" });

   const  onLogin = async () => {
    let hasUserName = userLogin.Username != undefined && userLogin.Username != "";
    let hasPassword = userLogin.Password != undefined && userLogin.Password != "";
    if (hasUserName && hasPassword) {
     await AuthService.loginService(userLogin);
    }else{
        alert("กรุณากรอกข้อมูลให้ครบถ้วน");
        return false;
    }
    props.history.push("/upload");
  }

  return (
    <div>
      <div className="boxlogin">
        <div className="imgprofile">
          <img src="https://images.unsplash.com/photo-1604988863474-3c7b8ae77f49?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1502&q=80"></img>
        </div>
        <div className="forml">
          <form>
            <div class="inputform">
              <label>UserName</label>
              <input
                type="text"
                className="inputcontrol"
                value={userLogin.Username}
                onChange={(ev) => {
                  setuserLogin((prevState) => ({
                    ...prevState,
                    Username: ev.target.value,
                  }));
                }}
              />
            </div>
            <div class="inputform">
              <label>Password</label>
              <input
                type="password"
                className="inputcontrol"
                value={userLogin.Password}
                onChange={(ev) => {
                  setuserLogin((prevState) => ({
                    ...prevState,
                    Password: ev.target.value,
                  }));
                }}
              />
            </div>
          </form>
        </div>
        <div className="btnlogin">
          <button
            className="btn-beautiful"
            onClick={(ev) => {
              onLogin(ev);
            }}
          >
            Login
          </button>
          <button
            className="btn-beautiful"
            onClick={(ev) => {
              onLogin(ev);
            }}
          >
            Clear
          </button>
          <div> </div>
        </div>
      </div>
    </div>
  );
}
