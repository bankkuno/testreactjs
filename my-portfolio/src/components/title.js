import React, { Component } from "react";
import "../css/title.css";
import { longdo, map, LongdoMap } from "./longdo";

const nameValidation = (fieldName, fieldValue) => {
  if (fieldValue.trim() === "") {
    return `${fieldName} is required`;
  }
  if (/[^a-zA-Z -]/.test(fieldValue)) {
    return "Invalid characters";
  }
  if (fieldValue.trim().length < 3) {
    return `${fieldName} needs to be at least three characters`;
  }
  return null;
};

const emailValidation = (email) => {
  if (
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      email
    )
  ) {
    return null;
  }
  if (email.trim() === "") {
    return "Email is required";
  }
  return "Please enter a valid email";
};

const ageValidation = (age) => {
  if (!age) {
    return "Age is required";
  }
  if (age < 18) {
    return "Age must be at least 18";
  }
  if (age > 99) {
    return "Age must be under 99";
  }
  return null;
};

const validate = {
  firstName: (name) => nameValidation("First Name", name),
  lastName: (name) => nameValidation("Last Name", name),
  email: emailValidation,
  age: ageValidation,
};

const initialValues = {
  age: 10,
  email: "no@email",
  firstName: "Mary",
  lastName: "Jane",
};

let useRefDict = {
  useRef1: React.createRef(),
  useRef2: null,
  useRef3: null,
};

export class Title extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apikey: "decfadddbb0791b6f4db8cd3303da1a6",
      firstName: "",
      lastName: "",
    
    };
  }
  
  initMap() {
    map.Layers.setBase(longdo.Layers.GRAY);
    map.Layers.add(longdo.Layers.TRAFFIC);
    console.log("map.Layers", map.Layers);

    map.Event.bind("overlayMove", function (overlay) {
      console.log(overlay.location());
    });
  }
  testAddMarker = () => {
    useRefDict.useRef1.focus();

    map.Route.add(
      new longdo.Marker(
        { lon: 100.538316, lat: 13.764953 },
        {
          title: "Victory monument",
          detail: "I'm here",
          detail: "Drag me",
          visibleRange: { min: 7, max: 9 },
          draggable: true,
        }
      )
    );
  };

  handleChange = (ev, emName) => {
    this.setState((prev) => {
      switch (emName) {
        case "firstName":
          return { ...prev, firstName: ev.target.value };
          break;
        case "lastName":
          return { ...prev, lastName: ev.target.value };
          break;
        default:
          break;
      }
    });
  };

  componentWillUnmount() {
    map.Event.unbind("overlayMove", function (overlay) {
      console.log(overlay.Marker.location);
    });
  }

  render() {
    return (
      <div>
        <form>
          <div className="row-group">
            <div className="form-group">
              <label htmlFor="first-name-input"> First Name * </label>

              <input
                type="text"
                className="form-control"
                id="first-name-input"
                placeholder="Enter first name"
                value={this.state.firstName}
                onChange={(ev) => this.handleChange(ev, "firstName")}
                name="firstName"
                required
              />
              {/* {touched.firstName && errors.firstName} */}
            </div>
            <div className="form-group">
              <label htmlFor="last-name-input"> Last Name * </label>

              <input
                type="text"
                className="form-control"
                id="last-name-input"
                placeholder="Enter last name"
                value={this.state.lastName}
                onChange={(ev) => this.handleChange(ev, "lastName")}
                name="lastName"
                required
              />
               <label htmlFor="last-name-input"> Last Name * </label>
              {/* {touched.firstName && errors.firstName} */}
            </div>

            <div className="form-group">
              <label htmlFor="first-name-input"> First Name * </label>

              <input
                type="text"
                className="form-control"
                id="first-name-input"
                placeholder="Enter first name"
                ref={(input) => {
                  useRefDict.useRef1 = input;
                }}
                value={this.state.firstName}
                onChange={this.handleChange}
                name="firstName"
                required
              />
              {/* {touched.firstName && errors.firstName} */}
            </div>
            <div className="form-group">
              <label htmlFor="first-name-input"> First Name * </label>

              <input
                type="text"
                className="form-control"
                id="first-name-input"
                placeholder="Enter first name"
                value={this.state.lastName}
                onChange={this.handleChange}
                name="firstName"
                required
              />
              {/* {touched.firstName && errors.firstName} */}
            </div>
          </div>
        </form>
        <button onClick={this.testAddMarker}>Test</button>
        <div>
          <label htmlFor="myInput">
            <h3>Test</h3>
            {/* <Icon style={{ fontSize: "20px" }} type="camera" /> */}
          </label>
          <input
            id="myInput"
            style={{ display: "none" }}
            type={"file"}
            onChange={this.fileSelectedHandler}
          />
        </div>
        <main>
          <LongdoMap
            id="longdo-map"
            mapKey={this.state.apikey}
            callback={this.initMap}
          />
        </main>
      </div>
    );
  }
}

export default Title;
