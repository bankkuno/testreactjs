import React, { Component } from "react";
import "../css/timeline.css";
export class Timeline extends Component {
  render() {
    return (
      <div>
        {/* <!-- The Timeline --> */}

        <ul className="timeline">
          {/* <!-- Item 1 --> */}
          <li>
            <div className="direction-r">
              <div className="flag-wrapper">
                <span className="flag">25 ธันวาคม</span>
                <span className="time-wrapper">
                  <span className="time">2020</span>
                </span>
              </div>
              <div className="desc">
                <ul>
                  <li>
                     ขึ้นบีทีเอสไปทำงานช่วง จาก บีทีเอสวงเวียนใหญ่ - บีทีเอสหมอชิต 
                  </li>
                  <li>
                    นั่งวินมอไซต์ไปตึก LPN 
                  </li>
                  <li>
                    ทำงานที่ตึก LPN
                  </li>
                  <li>
                   พักเที่ยงไปกินข้าวที่โรงอาหาร sun plaza
                  </li>
                  <li>
                    เลิกงานขึ้นบีทีเอส  จาก บีทีเอสหมอชิต -  บีทีเอสวงเวียนใหญ่ 
                  </li>
                  <li>
                    เดินไปซื้อข้าวที่เซเว่น หน้าปากซอย
                  </li>
                  <li>
                    กลับห้องพัก
                  </li>
                </ul>
              </div>
            </div>
          </li>

          {/* <!-- Item 2 --> */}
          <li>
            <div className="direction-l">
              <div className="flag-wrapper">
                <span className="flag">26 - 27 ธันวาคม</span>
                <span className="time-wrapper">
                  <span className="time">2020</span>
                </span>
              </div>
              <div className="desc">
                <ul>
                    <li>
                        ออกมาซื้อโจ๊กที่ตลาดวงเวียนใหญ่
                    </li>
                    <li>
                        เข้าเซเว่นเจริญรัถ
                    </li>
                    <li>
                        สั่ง Grab
                    </li>
                    <li>
                        ออกไปซื้อข้าวที่เซเว่น ลาดหญ้า 8
                    </li>
                    <li>
                        กลับห้อง
                    </li>
                </ul>
              </div>
            </div>
          </li>
             {/* <!-- Item 1 --> */}
          <li>
            <div className="direction-r">
              <div className="flag-wrapper">
                <span className="flag">28  ธันวาคม</span>
                <span className="time-wrapper">
                  <span className="time">2020</span>
                </span>
              </div>
              <div className="desc">
                <ul>
                  <li>
                     ขึ้นบีทีเอสไปทำงานช่วง จาก บีทีเอสวงเวียนใหญ่ - บีทีเอสหมอชิต 
                  </li>
                  <li>
                    นั่งวินมอไซต์ไปตึก LPN 
                  </li>
                  <li>
                    ทำงานที่ตึก LPN
                  </li>
                  <li>
                   พักเที่ยงไปกินข้าวที่โรงอาหาร sun plaza
                  </li>
                  <li>
                    เลิกงานขึ้นบีทีเอส  จาก บีทีเอสหมอชิต -  บีทีเอสวงเวียนใหญ่ 
                  </li>
                  <li>
                    เดินไปซื้อข้าวที่เซเว่น ซอยสารภี 3
                  </li>
                  <li>
                    กลับห้องพัก
                  </li>
                </ul>
              </div>
            </div>
          </li>
          <li>
            <div className="direction-l">
              <div className="flag-wrapper">
                <span className="flag">29  ธันวาคม</span>
                <span className="time-wrapper">
                  <span className="time">2020</span>
                </span>
              </div>
              <div className="desc">
                <ul>
                  <li>
                    - ขึ้นบีทีเอสไปทำงานช่วง จาก บีทีเอสวงเวียนใหญ่ - บีทีเอสหมอชิต 
                  </li>
                  <li>
                    นั่งวินมอไซต์ไปตึก LPN 
                  </li>
                  <li>
                    ทำงานที่ตึก LPN
                  </li>
                  <li>
                   พักเที่ยงไปกินข้าวที่โรงอาหาร sun plaza
                  </li>
                  <li>
                    เลิกงานขึ้นบีทีเอส  จาก บีทีเอสหมอชิต -  บีทีเอสคูคต
                  </li>
                  <li>
                    ซื้อของกินที่ Big C คลอง 4
                  </li>
                  <li>
                    กลับบ้าน
                  </li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}

export default Timeline;
