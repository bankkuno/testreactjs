import axios from "axios";

const urlBase = "baseURL";
const axiosService = axios.create({
  baseURL: urlBase,
  headers: {
    'x-apikey': '59a7ad19f5a9fa0808f11931',
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
}
});

export default axiosService;
